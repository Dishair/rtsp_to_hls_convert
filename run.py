#!/usr/bin/env python
# -*- coding: utf-8 -*-



import socket
import threading
import re
import subprocess
import time
from datetime import datetime
import psycopg
from array import *
from time import time
# import asyncio
# import json

subprocess.run(['killall ffmpeg'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
subprocess.run(['rm /var/www/html/*.m3u8'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
subprocess.run(['rm /var/www/html/*.ts'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)

conn = psycopg.connect(dbname='postgres', user='postgres', password='password', host='localhost')

# ffmpeg_run = (' -c:v copy -c:a copy -bufsize 1835k -flags -global_header -hls_time 10 -hls_wrap 10 -start_number 1 -hls_init_time 2 -hls_list_size 10 /var/www/html/video') # old

# ffmpeg_run = (' -use_wallclock_as_timestamps 1 -fflags nobuffer -rtsp_transport tcp  -c:v copy -an -flags -global_header -hls_time 1 -hls_list_size 1 -hls_flags independent_segments -hls_flags delete_segments /var/www/html/video') # low latency

ffmpeg_run = (' -fflags flush_packets -max_delay 5 -fflags nobuffer -rtsp_transport tcp  -flags -global_header -hls_time 2 -hls_list_size 2 -vcodec copy -y -hls_flags delete_segments /var/www/html/video') # works_fine

hikvision_reboot = ('curl -H "Content-Type: text/plain;charset=UTF-8\r\n" -H "Accept-Encoding: gzip, deflate\r\n" -H "Accept-Language: de,en-US;q=0.7,en;q=0.3\r\n" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0\r\n" --digest --http1.1 -X PUT http://') 



# Подключаемся к базе данных
def cam_list():
    # Open a cursor to perform database operations
    with conn.cursor() as cur:

        # Query the database and obtain data as Python objects.
        cur.execute("SELECT * FROM cams")
        record = cur.fetchall()

        # Make the changes to the database persistent
        conn.commit()
    return (record)



# Запрашиваем состояние потока и запускаем поток со всех камер
def check_rtsp_stream():
    cams = []
    threading.Timer(10.0, check_rtsp_stream).start()
    cam_list()
    for cam in cam_list():
        cam_id = (str((cam[0])))
        cam_url = (str((cam[1]))[2:-1])
        # PID процесса ffmpeg потока камеры в системе 
        cam_pid = (str((cam[2])))
        cam_state = ''
        cam_ip_short = ''
        cam_ip = ''
        cam_login = (str(cam[6])[2:-1])
        cam_pass = (str(cam[7])[2:-1])
        cam_manufacturer = (str(cam[8])[2:-1])
        cam_last_seen = (int((cam[9])))
        # print (cam_manufacturer)
        match = re.search('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', cam_url)
        if match:
            cam_ip = str(match[0])
        match = re.search('\d{1,3}\.\d{1,3}:554', cam_url)
        if match:
            cam_ip_short = str(match[0])[:-4]
            cam_ip_short = re.sub("\.","-",cam_ip_short)
            m3u8_path = 'video' + cam_ip_short + '.m3u8'
        rtsp_state = subprocess.run(['ffprobe -i ' + cam_url + ' 2>&1 | tail -n 1'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
        match = re.search(r'yuvj420p', rtsp_state.stdout)
        if match:
            cam_state = 'online'
            cam_last_seen = int(time())
            print ('cam last seen:' + str(cam_last_seen))
        else:
            cam_state = 'offline'
            subprocess.Popen(['/usr/bin/ffmpeg -v info -i ' + cam_url + ffmpeg_run + cam_ip_short + '.m3u8'], shell=True)
            # Время опроса камеры - 2 минуты
            if cam_manufacturer == "hikvision" and int(time()) - cam_last_seen >= 120:
                subprocess.Popen([hikvision_reboot  + cam_login + ':' + cam_pass + '@' + cam_ip + '/System/reboot'], shell=True)
            
        
        with conn.cursor() as cur:
            # Вставляем состояние камеры, там где совпадает ее ID 
            cur.execute("UPDATE cams  SET cam_state=%s, m3u8_path=%s, cam_last_seen=%s where cam_id=%s", (cam_state, m3u8_path, cam_last_seen, cam_id))
            conn.commit()
        json_string = '{ "cam_id":"' + cam_id + '", "cam_url":"' + cam_url + '", "cam_pid":"' + cam_pid + '", "cam_state":"' + cam_state + '", "m3u8_path":"' + m3u8_path + '"}'
        print (json_string)
        cams.append(json_string)

        #log = open("/etc/vid_streamer/service.log", "a")
        #log.write(str(cam_last_seen))
        #log.close()

    f = open("/etc/vid_streamer/cam_state.json", "w")
    cams = re.sub("'","",str(cams)) 
    print (cams)
    f.write(cam)
    f.close()
    return (cams)



check_rtsp_stream()


mySocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, proto=0, fileno=None)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # очищаем порт от использования чтобы избежать багов
print("Socket created.")
hostname = ''
portno = 9090
mySocket.bind((hostname, portno))
print("Socket bound to address {} and port number {}".format(hostname, portno))


while True:
    msg, client_addr = mySocket.recvfrom(1024)
    # print("Message received from the client:")
    # print(msg.decode())
    # print("Sending acknowledgment to the client.")
    f = open("/etc/vid_streamer/cam_state.json", "r")
    msg_out = str(check_rtsp_stream()).format(msg).encode()
    mySocket.sendto(msg_out, client_addr)
    # mySocket.close()
    # break
