A service that allows you to receive streams from cameras and give them in rtmp format and give them in HLS format for further embedding in the web.
Built using ffmpeg.

Functional:

  - automatic addition of cameras based on data from the database
  - automatic restart of the camera in case of problems with the stream
  - automatic restart of the stream after disconnection
